from rest_framework.generics import ListAPIView

from review.models import Review
from review.serializers import ReviewListSerializer


class ReviewListView(ListAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewListSerializer
