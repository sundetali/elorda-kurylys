from django.db import models


class Review(models.Model):
    full_name = models.CharField(max_length=250, verbose_name="фио")
    review = models.TextField(verbose_name="отзыв")
    from_review = models.CharField(max_length=150, null=True, blank=True, verbose_name="откуда")
    image = models.ImageField(upload_to="review", verbose_name="фото", null=True, blank=True)

    class Meta:
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"
