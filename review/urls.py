from django.conf.urls import url

from review.views import ReviewListView
urlpatterns = [
    url(r"^$", ReviewListView.as_view()),
]