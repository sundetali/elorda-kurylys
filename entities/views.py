from rest_framework.viewsets import ReadOnlyModelViewSet

from entities.models import Entity
from entities.serializers import EntitySerializer


class EntityViewSet(ReadOnlyModelViewSet):
    queryset = Entity.objects.all()
    serializer_class = EntitySerializer
