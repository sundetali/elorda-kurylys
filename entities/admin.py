from django.contrib import admin

from entities.models import Entity, Type, Investor, \
    Builder, Developer, Image, CCTV


class ImageAdmin(admin.TabularInline):
    model = Image


class CCTVAdmin(admin.TabularInline):
    model = CCTV


@admin.register(Entity)
class EntityAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    inlines = [ImageAdmin, CCTVAdmin]


@admin.register(Type)
class InvestorAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Builder)
class BuilderAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'type']


@admin.register(Developer)
class DeveloperAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Investor)
class InvestorAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'type']