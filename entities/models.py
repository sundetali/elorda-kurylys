from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Type(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Тип'
        verbose_name_plural = 'Типы'

    def __str__(self):
        return self.name


class Developer(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Застройщик'
        verbose_name_plural = 'Застройщики'

    def __str__(self):
        return self.name


class Builder(models.Model):
    name = models.CharField(max_length=100)
    type = models.ForeignKey('Type', on_delete=models.SET_NULL, related_name='builders', null=True)

    class Meta:
        verbose_name = 'Подрядчик'
        verbose_name_plural = 'Подрядчики'
        unique_together = ['name', 'type']

    def __str__(self):
        return '%s, %s' % (self.name, self.type)


class Investor(models.Model):
    name = models.CharField(max_length=100)
    type = models.ForeignKey('Type', on_delete=models.SET_NULL, related_name='investors', null=True)

    class Meta:
        verbose_name = 'Инвестор'
        verbose_name_plural = 'Инвесторы'
        unique_together = ['name', 'type']

    def __str__(self):
        return '%s, %s' % (self.name, self.type)


class Entity(models.Model):
    name = models.CharField(max_length=100, verbose_name='Наименование')
    name_kz = models.CharField(max_length=100, verbose_name='Наименование(kz)', null=True)
    image = models.ImageField(upload_to='entities/entity', null=True, blank=True)
    description = models.TextField(verbose_name='Местоположение')
    description_kz = models.TextField(verbose_name='Местоположение(kz)', null=True)
    readiness_text = models.CharField(max_length=150, verbose_name='Готовность')
    readiness_text_kz = models.CharField(max_length=150, verbose_name='Готовность(kz)', null=True)
    readiness_percent = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)],
                                            default=0, verbose_name='Готовность(%)')
    developer = models.ForeignKey('Developer', on_delete=models.SET_NULL, related_name='entities',
                                  null=True, blank=True, verbose_name='Застройщик')
    builder = models.ManyToManyField('Builder', verbose_name='Подрядчик', blank=True)
    investor = models.ManyToManyField('Investor', verbose_name='Инвестор', blank=True)
    deadline = models.CharField(max_length=100, verbose_name='Срок завершения')
    deadline_kz = models.CharField(max_length=100, verbose_name='Срок завершения(kz)', null=True)
    author_supervision = models.CharField(max_length=100, verbose_name='Авторский надзор')
    tech_supervision = models.CharField(max_length=100, verbose_name='Технический надзор')
    jsk = models.CharField(max_length=100, verbose_name='ЖСК', null=True, blank=True)
    jsk_kz = models.CharField(max_length=100, verbose_name='ЖСК(kz)', null=True, blank=True)
    land_plot = models.FloatField(verbose_name='Земельный участок')
    number_apartments = models.IntegerField(verbose_name='Количество квартир')
    number_holders = models.IntegerField(verbose_name='Количество дольщиков')
    number_spaces = models.IntegerField(verbose_name='Количество мест в паркинге',
                                        null=True, blank=True)
    total_area = models.DecimalField(decimal_places=2, verbose_name='Общая площадь', max_digits=10,
                                     null=True, blank=True)
    office_space = models.DecimalField(decimal_places=2, verbose_name='Площадь офисных помещений', max_digits=10,
                                       null=True, blank=True)
    communications = models.CharField(verbose_name="Наличие городских коммуникаций", max_length=100,
                                      null=True, blank=True)
    status = models.TextField(verbose_name='Статус', null=True, blank=True)
    status_kz = models.TextField(verbose_name='Статус(kz)', null=True, blank=True)
    event = models.TextField(verbose_name='Этапы мероприятий', null=True, blank=True)
    event_kz = models.TextField(verbose_name='Этапы мероприятий(kz)', null=True, blank=True)
    map = models.CharField(verbose_name='Ссылка на карту', null=True, blank=True, max_length=250)

    class Meta:
        verbose_name = 'Объект'
        verbose_name_plural = 'Объекты'

    def __str__(self):
        return self.name


class Image(models.Model):
    image = models.ImageField(upload_to='entities')
    entity = models.ForeignKey('Entity', on_delete=models.CASCADE, related_name='images')


class CCTV(models.Model):
    link = models.CharField(verbose_name='видеонаблюдение', null=True, blank=True, max_length=250)
    entity = models.ForeignKey('Entity', on_delete=models.CASCADE, related_name='videos')