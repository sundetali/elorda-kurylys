# Generated by Django 3.2.4 on 2021-07-13 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0009_auto_20210629_1227'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entity',
            name='assignee',
        ),
        migrations.RemoveField(
            model_name='entity',
            name='assignee_kz',
        ),
        migrations.AlterField(
            model_name='entity',
            name='description',
            field=models.TextField(verbose_name='Местоположение'),
        ),
        migrations.AlterField(
            model_name='entity',
            name='description_kz',
            field=models.TextField(null=True, verbose_name='Местоположение(kz)'),
        ),
        migrations.AlterField(
            model_name='entity',
            name='investor',
            field=models.ManyToManyField(blank=True, null=True, to='entities.Investor', verbose_name='Инвестор'),
        ),
    ]
