from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer
from entities.models import Entity, Developer, Builder, \
    Investor, Type, Image, CCTV
from elorda_kurylys.constants import lang_kz


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = '__all__'


class DeveloperSerialzier(serializers.ModelSerializer):
    class Meta:
        model = Developer
        fields = '__all__'


class BuilderSerializer(WritableNestedModelSerializer):
    type = TypeSerializer(allow_null=True)

    class Meta:
        model = Builder
        fields = '__all__'


class InvestorSerializer(serializers.ModelSerializer):
    type = TypeSerializer(allow_null=True)

    class Meta:
        model = Investor
        fields = '__all__'


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['id', 'image']


class CCTVSerializer(serializers.ModelSerializer):
    class Meta:
        model = CCTV
        fields = ['id', 'link']


class EntitySerializer(WritableNestedModelSerializer):
    developer = DeveloperSerialzier(allow_null=True)
    builder = BuilderSerializer(many=True)
    investor = InvestorSerializer(many=True)
    images = ImageSerializer(many=True)
    videos = CCTVSerializer(many=True)

    class Meta:
        model = Entity
        fields = [
            'id', 'name', 'image', 'images', 'description', 'readiness_text',
            'readiness_percent', 'developer', 'builder', 'investor', 'deadline',
            'author_supervision', 'tech_supervision', 'jsk', 'land_plot', 'number_apartments',
            'number_holders', 'number_spaces', 'total_area', 'office_space', 'communications',
            'status', 'event', 'videos', 'map'
        ]

    def to_representation(self, instance):
        lang = self.context['request'].query_params.get('lang')
        if isinstance(lang, str) and lang.lower() == lang_kz:
            instance.name = instance.name_kz
            instance.description = instance.description_kz
            instance.readiness_text = instance.readiness_text_kz
            instance.jsk = instance.jsk_kz
            instance.status = instance.status_kz
            instance.event = instance.event_kz

        return super(EntitySerializer, self).to_representation(instance)
