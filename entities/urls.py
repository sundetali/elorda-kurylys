from rest_framework.routers import DefaultRouter

from entities.views import EntityViewSet

router = DefaultRouter()
router.register(r'', EntityViewSet)
urlpatterns = router.urls