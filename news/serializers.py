from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer
from news.models import News, Image

from elorda_kurylys.constants import lang_kz


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['id', 'image']
        ref_name = 'news_image'


class NewsSerializer(WritableNestedModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = News
        fields = [
            'id', 'published', 'image', 'title', 'description',
            'link_video', 'images'
        ]

    def to_representation(self, instance):
        lang = self.context['request'].query_params.get('lang')

        if isinstance(lang, str) and lang.lower() == lang_kz:
            instance.title = instance.title_kz
            instance.description = instance.description_kz

        return super(NewsSerializer, self).to_representation(instance)
