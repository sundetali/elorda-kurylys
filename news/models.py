from django.db import models
from datetime import datetime


class News(models.Model):
    published = models.DateTimeField(verbose_name='Дата публикации', default=datetime.now, blank=True)
    image = models.ImageField(upload_to='news', null=True, blank=True)
    title = models.CharField(verbose_name='Заголовок', max_length=150)
    title_kz = models.CharField(verbose_name='Заголовок(kz)', max_length=150, null=True)
    description = models.TextField(verbose_name='Описание')
    description_kz = models.TextField(verbose_name='Описание(kz)', null=True)
    link_video = models.CharField(verbose_name='Ссылка на видео', max_length=250, null=True, blank=True)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['-published']

    def __str__(self):
        return self.title


class Image(models.Model):
    image = models.ImageField(upload_to="news")
    news = models.ForeignKey('News', on_delete=models.CASCADE, related_name="images")

