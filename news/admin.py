from django.contrib import admin

from news.models import News, Image


class ImageAdmin(admin.TabularInline):
    model = Image


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']
    inlines = [ImageAdmin]
