from rest_framework import serializers

from about.models import About


class AboutListSerializer(serializers.ModelSerializer):
    class Meta:
        model = About
        fields = '__all__'