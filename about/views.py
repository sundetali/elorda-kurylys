from rest_framework.generics import ListAPIView

from about.models import About
from about.serializers import AboutListSerializer


class AboutListView(ListAPIView):
    queryset = About.objects.all()
    serializer_class = AboutListSerializer
