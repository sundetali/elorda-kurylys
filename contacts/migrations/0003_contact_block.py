# Generated by Django 3.2.4 on 2021-07-07 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0002_auto_20210625_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='block',
            field=models.CharField(max_length=150, null=True, verbose_name='Блок'),
        ),
    ]
