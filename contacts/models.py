from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=150, verbose_name='Имя')
    phone = models.CharField(max_length=11, verbose_name='Номер Телефона', null=True)
    date = models.DateTimeField(auto_now_add=True, null=True, verbose_name='Дата заявки')
    block = models.CharField(max_length=150, null=True, verbose_name="Блок")
    mail = models.CharField(max_length=150, null=True, verbose_name="почта")

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return self.name
