from django.core.mail import send_mail
from django.conf import settings

from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from contacts.models import Contact
from contacts.serializers import ContactSerializer
from contacts.service import translate


class SendEmailView(CreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        data = serializer.validated_data
        recipient = data.pop('mail')
        data = translate(data)
        content = {"%s: %s" % (key, value) for (key, value) in data.items()}
        content = "\n".join(content)

        subject = 'Отзыв'
        message = content
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [recipient]
        send_mail(subject, message, email_from, recipient_list)

        return Response(serializer.data)
