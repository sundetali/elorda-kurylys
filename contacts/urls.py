from django.conf.urls import url

from contacts.views import SendEmailView

urlpatterns = [
    url(r'^send-mail/$', SendEmailView.as_view()),
]
