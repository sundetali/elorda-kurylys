def translate(data):
    new_data = dict()
    for key in data:
        if key == 'block':
            new_data['Блок'] = data[key]
        elif key == 'phone':
            new_data['Номер Телефона'] = data[key]
        elif key == 'name':
            new_data['Имя'] = data[key]
    return new_data
