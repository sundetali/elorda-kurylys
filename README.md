# Elorda-kurylys

## Installing

- Clone reposiroty `https://gitlab.com/sundetali/elorda-kurylys.git`
- Go to folder `elorda-kurykys`
- Install python3, pip
- Install virtualenv `pip3 install virtualenv` 
- Create virtual environ with name env `python3 -m venv env`
- Activate virtualenv `source env/bin/activate`
- Install project requirements `pip install -m requetemtns.txt`
- Install PostgreSQL(May use own database tool)
- Log into an interactive Postgre session 
- Create database `CREATE DATABASE elorda_kurylys;`
- Create user with password `CREATE USER elorda_user WITH PASSWORD 'elorda';`
- Grant all privileges to created user to administer the new database 
  `GRANT ALL PRIVILEGES ON DATABASE elorda_kurylys TO elorda_user;`
- Exit `\q`
- Copy .env.example to elorda_kurylys/.env file `cp .env.example elorda_kurylys/.env`
- Run all migrations `python manage.py migrate`
- Run the server `python manage.py runserver` 

