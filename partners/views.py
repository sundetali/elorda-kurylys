from rest_framework.generics import ListAPIView

from partners.models import Partner
from partners.serializers import PartnerSerializer


class PartnerListView(ListAPIView):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer