from django.db import models


class Partner(models.Model):
    image = models.ImageField(upload_to="partners")
    link = models.CharField(max_length=150, verbose_name="Ссылка")

    class Meta:
        verbose_name = "Партнер"
        verbose_name_plural = "Партнеры"
