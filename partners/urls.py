from django.conf.urls import url

from partners.views import PartnerListView

urlpatterns = [
    url(r'^$', PartnerListView.as_view())
]