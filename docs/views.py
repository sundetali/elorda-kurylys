from django.http import HttpResponse

from rest_framework.generics import ListAPIView, RetrieveAPIView

from .models import Agreement, Files
from .serializers import AgreementSerializer, FilesRetrieveSerializer


class AgreementListView(ListAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer


class FilesReadView(RetrieveAPIView):
    queryset = Files.objects.all()
    serializer_class = FilesRetrieveSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()

        with open(instance.file.path, 'rb') as pdf:
            response = HttpResponse(pdf.read(), content_type='application/pdf')
            response['Content-Disposition'] = 'inline;filename=' + instance.file.name + '.pdf'

        return response
