from django.urls import path

from docs.views import AgreementListView, FilesReadView

urlpatterns = [
    path('agreements/', AgreementListView.as_view()),
    path('files/<int:pk>/', FilesReadView.as_view()),
]