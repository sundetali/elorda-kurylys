from django.db import models


class Agreement(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True, verbose_name='Наименование')

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'


class Files(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    file = models.FileField()
    agreement = models.ForeignKey(Agreement, related_name='files', on_delete=models.CASCADE)
