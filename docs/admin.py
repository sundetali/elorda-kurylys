from django.contrib import admin

from .models import Agreement, Files


class FilesAdmin(admin.TabularInline):
    model = Files


@admin.register(Agreement)
class AgreementAdmin(admin.ModelAdmin):
    inlines = [FilesAdmin]
