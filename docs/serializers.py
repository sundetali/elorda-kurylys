from rest_framework import serializers

from .models import Agreement, Files


class FilesRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Files
        fields = ['id', 'name', 'file']


class AgreementSerializer(serializers.ModelSerializer):
    files = FilesRetrieveSerializer(many=True)

    class Meta:
        model = Agreement
        fields = ['id', 'name', 'files']
