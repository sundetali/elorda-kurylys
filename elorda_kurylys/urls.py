from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


from elorda_kurylys import settings

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contacts@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    url(r'admin/', admin.site.urls),

    url(r'api/entity/', include('entities.urls')),
    url(r'api/complexes/', include('sale.urls')),
    url(r'api/news/', include('news.urls')),
    url(r'api/contacts/', include('contacts.urls')),
    url(r'api/partners/', include('partners.urls')),
    url(r'api/docs/', include('docs.urls')),
    url(r'api/leadership', include('leadership.urls')),
    url(r'api/review/', include('review.urls')),
    url(r'api/ratings/', include('ratings.urls')),
    url(r'api/about/', include('about.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
