from django.conf.urls import url

from ratings.views import RatingListView
urlpatterns = [
    url(r"^$", RatingListView.as_view()),
]