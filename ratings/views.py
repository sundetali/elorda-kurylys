from rest_framework.generics import ListAPIView
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from ratings.models import Rating
from ratings.serializer import RatingSerializers
from ratings.filters import RatingFilterSet


class RatingListView(ListAPIView):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializers
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = RatingFilterSet
    ordering_fields = ["rating"]
