from django.db import models
from django.core.validators import MaxValueValidator


class Rating(models.Model):
    name = models.CharField(max_length=150, verbose_name="наименование")
    date = models.DateTimeField(verbose_name="дата")
    rating = models.PositiveIntegerField(validators=[MaxValueValidator(100)], verbose_name="рейтинг(%)")
    image = models.ImageField(null=True, blank=True, upload_to="ratings")

    class Meta:
        verbose_name = "рейтинг"
        verbose_name_plural = "рейтинг"
