from django_filters import rest_framework as filters
from django_filters.constants import EMPTY_VALUES

from ratings.models import Rating


class RangeFilter(filters.Filter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs
        value = value.split(",")
        return super(RangeFilter, self).filter(qs, value)


class RatingFilterSet(filters.FilterSet):
    date = RangeFilter("date", "range")

    class Meta:
        model = Rating
        fields = ["date"]
