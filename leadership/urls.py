from django.conf.urls import url

from leadership.views import LeaderShipListView
urlpatterns = [
    url(r'^$', LeaderShipListView.as_view()),
]