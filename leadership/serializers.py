from rest_framework import serializers

from leadership.models import LeaderShip


class LeaderShipListSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeaderShip
        fields = [
            'id', 'position', 'full_name', 'image',
            'email_link', 'phone_number'
        ]
