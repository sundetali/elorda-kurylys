from rest_framework.generics import ListAPIView

from leadership.models import LeaderShip
from leadership.serializers import LeaderShipListSerializer


class LeaderShipListView(ListAPIView):
    queryset = LeaderShip.objects.all()
    serializer_class = LeaderShipListSerializer
