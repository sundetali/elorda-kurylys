from django.contrib import admin

from leadership.models import LeaderShip


@admin.register(LeaderShip)
class LeaderShipAdmin(admin.ModelAdmin):
    list_display = ["id", "full_name", "position"]
