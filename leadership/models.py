from django.db import models


class LeaderShip(models.Model):
    position = models.CharField(max_length=250, verbose_name="должность")
    full_name = models.CharField(max_length=250, verbose_name="фио")
    image = models.ImageField(upload_to='leadership', verbose_name="фото", null=True, blank=True)
    email_link = models.CharField(max_length=250, verbose_name="почта", blank=True, null=True)
    phone_number = models.CharField(max_length=250, verbose_name="номер телефона", blank=True, null=True)

    class Meta:
        verbose_name = "Руководство"
        verbose_name_plural = "Руководство"
        ordering = ['id']
