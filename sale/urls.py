from django.conf.urls import url

from sale.views import ComplexesListView, ComplexesRetrieveView, \
    ComplexesTypeView, BuildingRetrieveView, TypeBuildingsView, StatusView

urlpatterns = [

    url(r'^$', ComplexesListView.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', ComplexesRetrieveView.as_view()),
    url(r'^type/(?P<buildings_type>[a-z_-]+)/$', ComplexesTypeView.as_view()),
    url(r'^(?P<complexes_id>[0-9]+)/building/(?P<pk>[0-9]+)/$', BuildingRetrieveView.as_view()),
    url(r'^types/$', TypeBuildingsView.as_view()),
    url('^statuses/$', StatusView.as_view()),
]
