from django.shortcuts import get_object_or_404

from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework import status

from django_filters.rest_framework import DjangoFilterBackend

from sale import SaleConstants
from sale.models import Complexes, Building, Status, TypeBuildings
from sale.serializers import ComplexesSerializer, \
    BuildingSerializer, StatusSerializer, TypeBuildingsSerialzier

from sale.filters import ComplexesFilter


class ComplexesListView(ListAPIView):
    queryset = Complexes.objects.all()
    serializer_class = ComplexesSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = ComplexesFilter


class ComplexesRetrieveView(RetrieveAPIView):
    queryset = Complexes.objects.all()
    serializer_class = ComplexesSerializer


class ComplexesTypeView(ListAPIView):
    queryset = Complexes.objects.all()
    serializer_class = ComplexesSerializer

    def list(self, request, *args, **kwargs):
        buildings_type = kwargs['buildings_type'].lower()
        print(buildings_type, "***")

        if buildings_type == 'residential':
            buildings_type = SaleConstants.residential
        elif buildings_type == 'non-residential':
            buildings_type = SaleConstants.non_residential
        elif buildings_type == 'parking':
            buildings_type = SaleConstants.parking
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
        queryset = self.filter_queryset(self.get_queryset())\
            .filter(type_buildings__name=buildings_type)
        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)


class BuildingRetrieveView(RetrieveAPIView):
    queryset = Building.objects.all()
    serializer_class = BuildingSerializer

    def retrieve(self, request, *args, **kwargs):
        complexes_id, pk = kwargs.get('complexes_id'), kwargs.get('pk')
        instance = get_object_or_404(Building, pk=pk, complexes_id=complexes_id)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class StatusView(ListAPIView):
    queryset = Status.objects.all()
    serializer_class = StatusSerializer


class TypeBuildingsView(ListAPIView):
    queryset = TypeBuildings.objects.all()
    serializer_class = TypeBuildingsSerialzier
