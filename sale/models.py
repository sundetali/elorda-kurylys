from django.db import models

from sale import SaleConstants


class TypeBuildings(models.Model):
    name = models.CharField(max_length=50, choices=SaleConstants.buildings_type)

    class Meta:
        verbose_name = 'Тип помещения'
        verbose_name_plural = 'Тип помещении'

    def __str__(self):
        return self.name


class Status(models.Model):
    name = models.CharField(max_length=50, choices=SaleConstants.statues)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'

    def __str__(self):
        return self.name


class Complexes(models.Model):
    name = models.CharField(max_length=100, verbose_name='Наименование')
    name_kz = models.CharField(max_length=100, verbose_name='Наименование(kz)', null=True)
    published = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации', null=True)
    image = models.ImageField(upload_to="sale/complexes", null=True, blank=True)
    address = models.CharField(max_length=100, verbose_name='Адресс')
    address_kz = models.CharField(max_length=100, verbose_name='Адресс(kz)', null=True)
    number_apartments = models.IntegerField(verbose_name='Количество квартир')
    price_from = models.DecimalField(max_digits=20, decimal_places=2,
                                     null=True, blank=True, verbose_name='Цена от')
    price_to = models.DecimalField(max_digits=20, decimal_places=2,
                                   null=True, blank=True, verbose_name='Цена до')
    date = models.DateField(verbose_name='Дата ввода объекта')
    type_buildings = models.ForeignKey('TypeBuildings', on_delete=models.SET_NULL,
                                       null=True, verbose_name='Тип помещения')
    status = models.ForeignKey('Status', on_delete=models.SET_NULL,
                               null=True, blank=True, related_name='buildings',
                               verbose_name='Статус')
    description = models.TextField(verbose_name='Описание', blank=True, null=True)
    description_kz = models.TextField(verbose_name='Описание(kz)', blank=True, null=True)
    location = models.CharField(verbose_name='Ссылка на карту', max_length=150, null=True, blank=True)
    location_kz = models.CharField(verbose_name='Ссылка на карту(kz)', max_length=150, null=True, blank=True)
    interior_decoration = models.TextField(verbose_name='Внутренняя отделка', null=True, blank=True)
    interior_decoration_kz = models.TextField(verbose_name='Внутренняя отделка(kz)', null=True, blank=True)
    beautification = models.TextField(verbose_name='Благоустройство', null=True, blank=True)
    beautification_kz = models.TextField(verbose_name='Благоустройство(kz)', null=True, blank=True)
    attractions = models.TextField(verbose_name='Достопримечательности', null=True, blank=True)
    attractions_kz = models.TextField(verbose_name='Достопримечательности(kz)', null=True, blank=True)

    class Meta:
        verbose_name = 'Комплексы'
        verbose_name_plural = 'Комплексы'
        ordering = ['-published']

    def __str__(self):
        return self.name


class Building(models.Model):
    name = models.CharField(max_length=100, verbose_name='Наименование')
    name_kz = models.CharField(max_length=100, verbose_name='Наименование(kz)', null=True)
    image = models.ImageField(null=True, blank=True)
    address = models.CharField(max_length=150, verbose_name='Адресс')
    address_kz = models.CharField(max_length=150, verbose_name='Адресс(kz)', null=True)
    floor = models.IntegerField(verbose_name='Этаж')
    price = models.DecimalField(max_length=20, decimal_places=2, max_digits=10,
                                null=True, blank=True, verbose_name='Цена')
    status = models.CharField(max_length=100, verbose_name='Статус')
    status_kz = models.CharField(max_length=100, verbose_name='Статус(kz)', null=True)
    description = models.TextField(verbose_name='Описание', null=True, blank=True)
    description_kz = models.TextField(verbose_name='Описание(kz)', blank=True, null=True)
    how_bought = models.TextField(null=True, blank=True, verbose_name='Как купить')
    how_bought_kz = models.TextField(null=True, blank=True, verbose_name='Как купить(kz)')
    complexes = models.ForeignKey('Complexes', on_delete=models.SET_NULL, related_name='buildings',
                                  verbose_name='Комплексы', null=True, blank=True)

    class Meta:
        verbose_name = 'Помещение'
        verbose_name_plural = 'Помещение'

    def __str__(self):
        return self.name


class Image(models.Model):
    image = models.ImageField(upload_to='sale/building')
    building = models.ForeignKey('Building', on_delete=models.CASCADE, related_name='images')
