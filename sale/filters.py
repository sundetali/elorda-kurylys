from django_filters import rest_framework as filters

from sale.models import Complexes


class ComplexesFilter(filters.FilterSet):
    price_from = filters.NumberFilter(field_name='price_from', lookup_expr='gte')
    price_to = filters.NumberFilter(field_name='price_to', lookup_expr='lte')
    type_buildings = filters.CharFilter(field_name='type_buildings__name')
    status = filters.CharFilter(field_name='status__name')

    class Meta:
        model = Complexes
        fields = ['type_buildings', 'status', 'price_from', 'price_to']
