from django.contrib import admin

from sale.models import Image, Building, Status, TypeBuildings, Complexes


class ImageAdmin(admin.TabularInline):
    model = Image


@admin.register(Building)
class BuildingAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    inlines = [ImageAdmin]


@admin.register(Complexes)
class ComplexesAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(TypeBuildings)
class StatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
