# Generated by Django 3.2.4 on 2021-06-29 06:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sale', '0005_alter_typebuildings_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='complexes',
            name='address_kz',
            field=models.CharField(max_length=100, null=True, verbose_name='Адресс(kz)'),
        ),
        migrations.AddField(
            model_name='complexes',
            name='description_kz',
            field=models.TextField(null=True, verbose_name='Описание(kz)'),
        ),
        migrations.AddField(
            model_name='complexes',
            name='location_kz',
            field=models.CharField(blank=True, max_length=150, null=True, verbose_name='Адресс(kz)'),
        ),
        migrations.AddField(
            model_name='complexes',
            name='name_kz',
            field=models.CharField(max_length=100, null=True, verbose_name='Наименование(kz)'),
        ),
    ]
