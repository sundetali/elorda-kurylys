# Generated by Django 3.2.4 on 2021-06-29 06:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sale', '0006_auto_20210629_1247'),
    ]

    operations = [
        migrations.AddField(
            model_name='building',
            name='address_kz',
            field=models.CharField(max_length=150, null=True, verbose_name='Адресс(kz)'),
        ),
        migrations.AddField(
            model_name='building',
            name='description_kz',
            field=models.TextField(null=True, verbose_name='Описание(kz)'),
        ),
        migrations.AddField(
            model_name='building',
            name='how_bought_kz',
            field=models.TextField(blank=True, null=True, verbose_name='Как купить(kz)'),
        ),
        migrations.AddField(
            model_name='building',
            name='name_kz',
            field=models.CharField(max_length=100, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='building',
            name='status_kz',
            field=models.CharField(max_length=100, null=True, verbose_name='Статус(kz)'),
        ),
    ]
