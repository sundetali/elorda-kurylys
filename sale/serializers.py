from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer

from elorda_kurylys.constants import lang_kz
from sale.models import Status, TypeBuildings, Complexes, Building, Image


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ['id', 'name']


class TypeBuildingsSerialzier(serializers.ModelSerializer):
    class Meta:
        model = TypeBuildings
        fields = '__all__'


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['id', 'image']
        ref_name = "buildings_image"


class BuildingsTranslateSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Building
        fields = [
            'id', 'name', 'name_kz', 'image',
            'address', 'address_kz', 'floor', 'price',
            'status', 'status_kz', 'description', 'description_kz',
            'how_bought', 'how_bought_kz', 'images'
        ]


class BuildingSerializer(BuildingsTranslateSerializer, WritableNestedModelSerializer):

    class Meta(BuildingsTranslateSerializer.Meta):
        fields = [
            'id', 'name', 'image', 'address', 'floor',
            'price', 'status', 'description', 'how_bought',
            'images'
        ]

    def to_representation(self, instance):
        lang = self.context['request'].query_params.get('lang')
        if isinstance(lang, str) and lang.lower() == lang_kz:
            instance.name = instance.name_kz
            instance.address = instance.address_kz
            instance.status = instance.status_kz
            instance.description = instance.description_kz
            instance.how_bought = instance.how_bought_kz

        return super(BuildingSerializer, self).to_representation(instance)


class ComplexesSerializer(WritableNestedModelSerializer):
    status = StatusSerializer()
    type_buildings = TypeBuildingsSerialzier()
    buildings = BuildingsTranslateSerializer(many=True)

    class Meta:
        model = Complexes
        fields = [
            'id', 'name', 'image', 'address',
            'number_apartments', 'price_from', 'price_to', 'date',
            'status', 'description', 'interior_decoration', 'beautification',
            'attractions', 'location', 'buildings', 'type_buildings',
        ]

    def to_representation(self, instance):
        lang = self.context['request'].query_params.get('lang')
        if isinstance(lang, str) and lang.lower() == lang_kz:
            instance.name = instance.name_kz
            instance.address = instance.address_kz
            instance.description = instance.description_kz
            instance.location = instance.location_kz
            instance.interior_decoration = instance.interior_decoration_kz
            instance.beautification = instance.beautification_kz
            instance.attractions = instance.attractions_kz

        return super(ComplexesSerializer, self).to_representation(instance)
