class SaleConstants:
    # room types
    residential = 'жилое'
    non_residential = 'нежилое'
    parking = 'паркинг'

    buildings_type = (
        (residential, 'жилое'),
        (non_residential, 'нежилое'),
        (parking, 'паркинг')
    )

    # statuses
    openn = 'Открыто'
    closed = 'Закрыто'
    expects = 'Ожидает'

    statues = (
        (openn, openn),
        (closed, closed),
        (expects, expects)
    )
